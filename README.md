# Camagru
## An image sharing web application built in PHP for the Apache Web Server and MySQL

## Overview

### Stack

#### Backend

* Apache
* PHP
* MySQL

#### Frontend

* JavaScript
* HTML
* CSS
* Bootstrap CSS

### Configuration

#### Clone the repository

> git clone https://bitbucket.org/blavkboy/camagru

> cd camagru

#### Setup up database tables
> php config/setup.php

#### Visit the application
* Open [camagru](http://127.0.0.1:8080/camagru)

#### What you are presented with

* The feed will initially be empty so you will need to register a user
* Once registered, visit your email inbox to get the verification link
* The application should now have you authenticated and you can upload images
	* (Optional) You can take a picture using the WebRTC API with your webcam, should you have one on your computer.
	* (Optional) You can apply a sticker to your image.
* Navigate the application as an authenticated user and you are then able to do the following:
	* Comment on user images.
	* Like user images.
	* Delete your images.
	* Update your profile. (Caution) updating your email will require you to verify that email.
